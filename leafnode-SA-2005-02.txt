leafnode-SA-2005:02.fetchnews-hangs-on-header

Topic:		potential denial of service in leafnode

Announcement:	leafnode-SA-2005:02
Author:		Matthias Andree
Version:	1.00
Announced:	2005-06-08
Category:	main
Type:		potential denial of service
Impact:		fetchnews hangs, no new fetchnews/texpire processes
		can be started
Credits:	Adam Funk (bug report)
Danger:		medium:
		- no build-up of memory consumption
		- no privilege escalation through this bug
		- malicious upstream server can be unlisted
CVE Name:	CVE-2005-1911
URL:		http://www.leafnode.org/leafnode-SA-2005-02.txt
		http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2005-1911

Affects:	leafnode versions up to and including 1.11.2

Not affected:	leafnode 1.11.3

Default install: affected.

Corrected:	2005-06-08 14:06 UTC (CVS) - committed corrected version
		2005-06-08                   leafnode 1.11.3 released

0. Release history

2005-06-08	1.00 initial announcement

1. Background

leafnode is a store-and-forward proxy for Usenet news, is uses the
network news transfer protocol (NNTP). It consists of several
collaborating programs, the server part is usually started by inetd,
xinetd or tcpserver, the client part is usually started by cron,
a PPP post-connect script or manually.

This security announcement pertains to leafnode-1, the stable branch.

The leafnode-2 development branch is not subject to security announcements.

2. Problem description

A vulnerability was found in the fetchnews program (the NNTP client) that
may under some circumstances cause a wait for input that never arrives,
fetchnews "hangs". This hang does not cost CPU.

3. Impact

As only one fetchnews program can run at a time, subsequently started
fetchnews and texpire programs will terminate. This means that the news
database will no longer be updated, older articles will no longer
expire, until the hanging fetchnews process gets unstuck, usually
through a manual "kill" command or a reboot.

4. Workaround

Comment out all configuration pertaining to the malicious server.

Note that this is not a full solution as transient network errors can
also cause delays in querying other network servers, and it requires
manual intervention to find out which server is malicious.

5. Solution

Upgrade your leafnode package to version 1.11.3.
leafnode 1.11.3 is available from SourceForge:
<http://sourceforge.net/project/showfiles.php?group_id=57767>

Leafnode 1.X versions are deemed stable, and it is usually best to go
for the latest released 1.X version to have all the other bug fixes as
well.

A. References

leafnode home page: <http://www.leafnode.org/>

B. Copyright and License

(C) Copyright 2005 by Matthias Andree, <matthias.andree@gmx.de>.
Some rights reserved.

This work is licensed under the Creative Commons
Attribution-NonCommercial-NoDerivs German License. To view a copy of
this license, visit http://creativecommons.org/licenses/by-nc-nd/2.0/de/
or send a letter to Creative Commons; 559 Nathan Abbott Way;
Stanford, California 94305; USA.

END OF leafnode-SA-2005:02.fetchnews-hangs-on-header
