/* (C) 2002 - 2021 by Matthias Andree
 *
 * This file is under the same license as the rest of leafnode. Please see the
 * file "COPYING" that should be in the same directory as this.
 */

#include "leafnode.h"

#include <stdio.h>
#include <string.h>

int main(int argc, char **argv) {
    static char env_path[] = "PATH=/bin:/usr/bin";
    uint8_t pcre2info[64]; /* PCRE 10 requires at least 24 code units */

    /* DO NOT CHANGE THE OUTPUT FORMAT; EXTERNAL TOOLS DEPEND ON IT!
     * (namely, leafwa does)
     */
    fputs("version: leafnode-", stdout);
    puts(version);

    /* new in 1.11.7: -v mode to print more information, such as directories */
    if (argc > 1 &&
	    (0 == strcmp(argv[1], "-v")
	     || 0 == strcmp(argv[1], "--verbose")))
    {
	fputs("current machine: ", stdout);
	fflush(stdout);
	putenv(env_path);
	if (system("uname -a"))
	    puts(" (error)");
	fputs("sysconfdir: ", stdout);
	puts(sysconfdir);
	fputs("spooldir: ", stdout);
	puts(spooldir);
	fputs("lockfile: ", stdout);
	puts(lockfile);
#ifdef HAVE_IPV6
	puts("IPv6: yes");
#else
	puts("IPv6: no");
#endif
#ifdef HAVE_GETIFADDRS
	puts("HAVE_GETIFADDRS: yes");
#else
	puts("HAVE_GETIFADDRS: no");
#endif
	pcre2_config_8(PCRE2_CONFIG_VERSION, pcre2info);
	printf("PCRE2 version: %s\n", pcre2info);

	pcre2_config_8(PCRE2_CONFIG_UNICODE_VERSION, pcre2info);
	printf("PCRE2 Unicode version: %s\n", pcre2info);
    }
    return 0;
}
