/* (C) Copyright 2002 - 2021 Matthias Andree, see COPYING for license. */

#include <string.h>

#include "config.h"
#include "groupselect.h"
#include "ln_log.h"

pcre2_code_8 *gs_compile(const unsigned char *regex) {
    int regex_errcode;
    size_t regex_errpos;
    pcre2_code_8 *re;

    if (NULL == (re = pcre2_compile_8(regex, PCRE2_ZERO_TERMINATED, PCRE2_MULTILINE, &regex_errcode,
				      &regex_errpos, NULL))) {
	unsigned char regex_errmsg[1024];
	int len = pcre2_get_error_message_8(regex_errcode, regex_errmsg, sizeof(regex_errmsg));
	ln_log(LNLOG_SERR, LNLOG_CTOP, "Invalid group pattern "
		"in \"%s\" at char #%lu: %s%s", regex, (unsigned long)regex_errpos, regex_errmsg,
	       len == PCRE2_ERROR_NOMEMORY ? "[...]" : "");
    }
    return re;
}

/* match s against PCRE p
 * WARNING: If p is NULL, every string s is considered a match
 * returns 1 for match, 0 for mismatch, negative for error */
int gs_match(const pcre2_code_8 *re, const unsigned char *s) {
    int match;
    pcre2_match_data_8 *match_data;

    if (re == NULL) return 1;
    match_data = pcre2_match_data_create_8(1, NULL);
    if (NULL == match_data) {
	    ln_log(LNLOG_SERR, LNLOG_CTOP, "gs_match: out of memory allocating match_data");
	    return -1;
    }

    match = pcre2_match_8(re, s, PCRE2_ZERO_TERMINATED, 0,
	    PCRE2_ANCHORED, match_data, NULL);

    pcre2_match_data_free_8(match_data);
    if (match == PCRE2_ERROR_NOMATCH) return 0;
    if (match >= 0) return 1;
    return match; /* match < 0, but not PCRE2_ERROR_NOMATCH */
}
