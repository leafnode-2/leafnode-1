/* ln_pcre_extract.c -- compile and exec a PCRE and return captured strings
 * (C) 2002 - 2021 by Matthias Andree <matthias.andree@gmx.de>
 *
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of version 2 of the GNU Lesser General Public License as
 * published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library (look for the COPYING.LGPL file); if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 */

#include "leafnode.h"

#include <string.h>
#include <limits.h>
#ifdef TEST
#include <stdio.h>
#endif

#include "config.h"
#include "critmem.h"
#include "ln_log.h"
#include "strlcpy.h"

/* This function compiles and executes the regular expression in
 * "pattern" against the string in "input" and can optionally put the
 * matching string and captured substring into the output vector.
 *
 * Output may be NULL if and only if num is zero.
 *
 * If num is nonzero, the matching substring is strdup()ed into
 * output[0]. If num is > 1, ln_pcre_extract copies the first num - 1
 * captured substrings into output[...]. If a capturing subpattern did
 * not match, the corresponding output[] element is NULL.
 *
 * The return value is the same as that of pcre_exec. Short: 0 if num
 * too small to hold all captured strings, otherwise the number of
 * captured patterns, where the matching string is always captured, or
 * the PCRE error code.
 *
 * WARNING: if compiling the pattern fails, the program logs this
 * condition and aborts. This function is NOT safe to be used with
 * user-accessible patterns. Use constant patterns.
 *
 * You MUST free() the valid parts in your output[] vector yourself. You
 * can use ln_pcre_extract_free (which see) to do this.
 */
int ln_pcre_extract(const unsigned char *input, const unsigned char *pattern, char **output, size_t num)
{
    size_t errpos, i;
    int errcode, match;
    const int options = PCRE2_CASELESS|PCRE2_DOTALL|PCRE2_MULTILINE;
    PCRE2_SIZE *ovec;
    pcre2_code_8 *re;
    pcre2_match_data_8 *match_data;

    if (num > INT_MAX / 3) {
	ln_log(LNLOG_SERR, LNLOG_CTOP, "ln_pcre_extract: array size too large, aborting.");
	abort();
    }

    re = pcre2_compile_8(pattern, PCRE2_ZERO_TERMINATED, options, &errcode, &errpos, NULL);
    if (NULL == re) {
	unsigned char errstr[SIZE_lineout];
	int len = pcre2_get_error_message_8(errcode, errstr, sizeof(errstr));
	ln_log(LNLOG_SERR, LNLOG_CTOP, "ln_pcre_extract: cannot compile \"%s\": %s%s at pos. #%lu",
	       pattern, errstr, len == PCRE2_ERROR_NOMEMORY ? "[...]" : "", (unsigned long)errpos);
	return -1;
    }

    match_data = pcre2_match_data_create_from_pattern_8(re, NULL);
    if (NULL == match_data) {
	ln_log(LNLOG_SERR, LNLOG_CTOP, "ln_pcre_extract: out of memory allocating match_data");
	return -1;
    }

    match = pcre2_match_8(re, input, PCRE2_ZERO_TERMINATED, 0, 0, match_data, NULL);
    if (match >= 0) {
	uint32_t ocnt = pcre2_get_ovector_count_8(match_data);
	ovec = pcre2_get_ovector_pointer_8(match_data);
	for (i = 0 ; i < num && i < ocnt ; i++) {
		size_t l = ovec[i*2 + 1] - ovec[i*2];
		if (ovec[i*2 + 1] < ovec[i * 2]) l = 0; /* can happen, for instance, with \K */
		output[i] = critmalloc(l + 1, "ln_pcre_extract");
		(void)memcpy(output[i], input + ovec[i*2], l);
		output[i][l] = '\0';
	}
    }
    pcre2_match_data_free_8(match_data);
    pcre2_code_free_8(re);
    return match;
}

/* free a vector as allocated by ln_pcre_extract
 * vec should be the same as output in ln_pcre_extract
 * count should be the value obtained from ln_pcre_extract
 * it is safe to pass a negative or zero count.
 *   
 * vec must NOT be NULL unless count is zero or negative.
 */
void ln_pcre_extract_free(char **vec, int count)
{
    int i;

    for (i = 0; i < count; i++) {
	if (vec[i]) {
	    free(vec[i]);
	    vec[i] = NULL;
	}
    }
}

#ifdef TEST
#define MAX 30

int debug = 0;
int verbose = 0;

/* test ln_pcre_extract capturing. */
int main(int argc, char **argv)
{
    char *out[MAX]; /* RATS: ignore */
    int rc, n = MAX;

    if (argc < 3 || argc > 4) {
	fprintf(stderr, "usage: %s string PCRE [num]\n", argv[0]);
	exit(1);
    }

    if (argc == 4) {
	n = atoi(argv[3]);
	if (n > MAX) {
	    n = MAX;
	    printf("warning: clamping max from %d to %d\n", n, MAX);
	}
    }
    rc = ln_pcre_extract((unsigned char *)argv[1], (unsigned char *)argv[2], out, argc == 4 ? atoi(argv[3]) : MAX);
    printf("ln_pcre_extract returned %d\n", rc);
    if (rc >= 0) {
	int i;
	for(i = 0; i < rc; i++) {
	    printf("substring #%d: \"%s\"\n", i, out[i] ? out[i] : "(NULL)");
	}
    }
    ln_pcre_extract_free(out, rc);

    exit(0);
}
#endif
