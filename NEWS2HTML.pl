#! /usr/bin/perl -wT

use strict;
my $hr = 0;
my $liopen = 0;

warn "Reading from TTY" if (@ARGV == 0) and (-t STDIN);

while(<>) {
    next if (/^\s*$/);

    if (/====/) {
	last if (++$hr == 2);
	next;
    }

    if (/^\* ([^,]+), ([0-9-]+)/) {
	print "<h3>$1 (released $2)</h3>\n";
	next;
    }

    if (/^- (.*)$/) {
	if ($liopen == 0) {
	    $liopen++;
	    print "<ul>\n";
	} else {
	    print "</li>";
	}
	print "<li>$1\n";
	next;
    }

    if (/^  (.*)$/) {
	print "    $1\n";
	next;
    }

    if ($liopen) {
	print "</li></ul>\n";
	$liopen = 0;
    }

    if (/^### (.*)$/) {
	print "\n<h4>$1</h4>\n\n";
	next;
    }

    print "<p>$_";
}

if ($liopen) {
	print "</li></ul>\n";
	$liopen = 0;
    }


