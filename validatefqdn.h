#ifndef VALIDATEFQDN_H
#define VALIDATEFQDN_H

/* for validatefqdn.c */
int is_validfqdn(const char *fqdn);
void validatefqdn(int);

#endif
