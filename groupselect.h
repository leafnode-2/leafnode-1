#ifndef GROUPSELECT_H
#define GROUPSELECT_H

/* for pcre type */
#define PCRE2_CODE_UNIT_WIDTH 0
#include <pcre2.h>

pcre2_code_8 *gs_compile(const unsigned char *regex);
int gs_match(const pcre2_code_8 *p, const unsigned char *s);

#endif
