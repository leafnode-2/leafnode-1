#! /bin/sh
set -ex
cd "$(dirname "$0")"
cp -pf ../NEWS NEWS.txt
#recode <../NEWS >NEWS.txt iso-8859-1..UTF-8
rsync -Hrlt -vP -e ssh \
* \
m-a,leafnode@web.sourceforge.net:/home/groups/l/le/leafnode/htdocs/ \
--delete-excluded --delete --exclude "*~" \
"$@" &
rsync -Hrlt -vP * ~/public_html/leafnode/nw/ \
--delete-excluded --delete --exclude "*~" \
"$@" &
rsync -Hrlt -vP * rsv1.an3e.de:/usr/local/www/leafnode.org/ \
--delete-excluded --delete --exclude "*~" \
"$@" &
wait
